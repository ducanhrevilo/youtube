var express = require('express');
var router = express.Router();
const axios = require('axios');
//const qr = require('qr-image');   
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Heloo to my app' });
});

router.get('/loginpass', function(req, res, next) {
  res.render('loginpass', { title: 'loginpass',message:'Search name of the song you like'})
});



router
  .use('/users', (req,res, next) => {
    if(!req.user){
      res.redirect('/')
    }
    next()
  })
  .get('/users', (req, res) => {
    axios.get(req.user.media)
    .then(function (response) {
      const data = response.data.data;
      let user = req.user;
      user.images = data.map(img => img.images);
      user.link = data.map(lk => lk.link);
          res.render('instagram', user)  

    })
  })

module.exports = router;
