const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const passport = require('passport');
 const passportfb = require('passport-instagram').Strategy;
 const session = require('express-session');
const axios = require('axios');

const indexRouter = require('./routes/');
//const users = require('./routes/users');
const app = express();

 app.use(session({
   secret:"aasdasf"
 }))
 app.use(passport.initialize())
 app.use(passport.session())
 app.get('/auth/fb', passport.authenticate('instagram'));
 app.get(
   '/auth/fb/cb',
   passport.authenticate('instagram', {
     successRedirect: '/loginpass',
     failureRedirect: '/loginpass'
   })
 );

 passport.use(new passportfb(
   {
     clientID:"59d9d54169e7420392b36b7d9f088830",
     clientSecret:"c642c3724d834ad1a30c7a01ebc5ae87",
     callbackURL: "http://54.91.37.209:3000/auth/fb/cb",
     //profileFields: ['id','gender']
   },
   (accessToken, refreshToken, profile, done) => {
     console.log(profile);
     let user = {};
      user.name = profile.displayName;
      user.homePage = profile._json.data.website;
      user.image = profile._json.data.profile_picture;
      user.bio = profile._json.data.bio;
      user.media = `https://api.instagram.com/v1/users/${profile.id}/media/recent/?access_token=${accessToken}&count=8`
      done(null, user)  
  }))

 passport.serializeUser((user,done) =>{
   done(null,user)
 })

 passport.deserializeUser((user, done) => {
    done(null, user)
  })


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
//app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
